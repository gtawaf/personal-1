<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerformanceReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_reviews', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id');
            $table->foreignId('reviewer_id');
            $table->enum('rating', [1, 2, 3, 4, 5]);
            $table->longText('comments')->nullable();
            $table->timestamp('creation_time');

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('reviewer_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_reviews');
    }
}
