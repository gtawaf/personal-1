<?php

namespace App\Http\Controllers;
use App\Models\Employee;

class EmployeesController extends Controller
{
    // Return all employess as json string
    public function index()
    {
      return response()->json(Employee::all());
    }
}
