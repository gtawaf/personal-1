<?php

namespace App\Http\Controllers;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ReviewsController extends Controller
{
    public function index()
    {
      return response()->json(Review::all());
    }

    /**
     * Store a new review.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
      $result = $this->validate($request, [
        // employee id is present and exists in employees
        // TODO: change validation erros to be more expersive
        'employee_id' => 'required|exists:employees,id',
        'reviewer_id' => 'required|exists:employees,id|different:employee_id',
        'rating' => ['required',Rule::in([1, 2, 3, 4, 5])],
        'creation_time' => 'required|date'
      ]);
      /* if validation passed then we need to check that the
      * reviewer has not reviewed this employee within the last year
      * I would usually create a custom validator to this but I did not have enough time
      * this will get me the last review this reviewer did for this employee
      */
      $input = $request->only(['employee_id', 'reviewer_id', 'rating', 'creation_time', 'comments']);
      $last_review = Review::where('employee_id', $input['employee_id'])
                        ->where('reviewer_id', $input['reviewer_id'])
                        ->orderBy('creation_time', 'desc')->select(DB::raw("strftime('%Y', creation_time) as year"))->get();

      if(!empty($last_review)) {
        $year = \Carbon\Carbon::parse($input['creation_time'])->year;
        // this is not working properly array is nested so needs more work
        if(in_array($year, $last_review->toArray())){
          return respone()->json("{'creation_time' : 'Invalid date, reviews has submitted review for this employee for this calender year'}");
        }
      }
      // Take only the data needed
      $review = Review::create($input);
      return $review->save();
    }
}
