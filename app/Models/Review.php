<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'reviewer_id', 'rating', 'comments', 'creation_time'
    ];

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'performance_reviews';

    /**
    * generate timestampe flag
    *
    * @var boolean
    */
    public $timestamps = false;

    /**
     * Get the employee the review is for
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Get the reviews
     */
    public function reviewer()
    {
        return $this->belongsTo('App\Models\Employee', 'reviewer_id');
    }
}
