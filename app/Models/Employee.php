<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Employee', 'employee_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function reviewed()
    {
        return $this->hasMany('App\Models\Employee', 'reviewer_id');
    }
}
