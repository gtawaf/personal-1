<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'This is an API, please go to API End points';
});

$router->group(['prefix' => 'api'], function () use ($router) {
    # test controller
    $router->get('test', ['as' => 'test', 'uses' => 'FirstController@test']);
    $router->get('employees', ['as' => 'employees', 'uses' => 'EmployeesController@index']);
    $router->get('reviews', ['as' => 'reviews', 'uses' => 'ReviewsController@index']);
    $router->post('reviews', ['as' => 'create_review', 'uses' => 'ReviewsController@store']);

});
