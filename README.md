# Checkout 51 Challenge:
 - This was build using Lumen Laravel based framework
 - to run tests please run phpunit
 - to run server
      php -S localhost:[port] -t public
- Created an api prefix to have all of our REST API endpoints inside it
- database being used is sqlite but you deicde which one you would like
- employess list http://localhost:8000/api/employees
- review create and get http://localhost:8000/api/reviews
